# 1.0.0 (2019-10-31)


### Features

* Add gman image ([5f7a4e9](https://gitlab.com/dreamer-labs/piperci/pytest-docker-fixtures/commit/5f7a4e9))
